# Ultimate Dashboard for Jira

This is a public support repository, primarily used as an issue tracker for the
Ultimate Dashboard for Jira application.

## Support Links

- [Support Portal]
- [Documentation]
- [Main Website]

[Support Portal]: https://ultimatejiradashboard.atlassian.net/servicedesk/customer/portals
[Documentation]: https://ultimatejiradashboard.atlassian.net/wiki/spaces/SUP/overview
[Main Website]: https://ultimatejiradashboard.com/
